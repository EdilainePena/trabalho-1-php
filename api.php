<?php
include ("./index.html");

    function VeriSoma($x, $y, $z){
        $epsilon = 0.00001;
        if((((($x + $y) + $z) - ($x + ($y + $z))) < $epsilon) 
            and $x + $y == $y + $x and $x + 0 == $x and $x + (-$x) == 0)
            return true;
        else
            return false;
    }

    function VeriProduto($x, $y, $z){
        if((($x * $y) * $z == $x * ($y * $z) and $x * pow($x, -1) == 1 
        and $y * pow($y, -1) == 1 and  $x * 1 == $x and $y * 1 == $y
        and $x * $y == $y * $x and $x !=0 and $y != 0) or $x == 0 or $y == 0)
            return true;
        else
            return false;
    }

    function Soma($x, $y){
        $z = $x + $y; 
        if(VeriSoma($x,$y,$z))
            return $z;
        else
            echo "Axiomas da soma não foram respeitados!!!";
    }

    function Produto($x, $y){
        $z = $x * $y; 
        if(VeriProduto($x,$y,$z))
            return $z;
        else
            echo "Axiomas da multiplicaçao não foram respeitados!!!";
    }
    
    $x = ($_GET ['x']);
    $y = ($_GET ['y'] );

    if(is_numeric($x) and is_numeric($y)){
        echo '<br/n>', "Soma: ", $x, " + ", $y, " = ", Soma($x, $y), '<br/n>';
        echo '<br/n>', "Produto: ", $x, " * ", $y, " = ", Produto($x, $y);
    }
    else{
        echo "Preencha todos os campos corretamente! <br> Obs: Utilize pontos para numeros decimais"; 
    }
   

?>